﻿using UnityEditor;
using UnityEngine;

public class SpaceCamera : MonoBehaviour
{
  private void Start()
  {
    var guids = AssetDatabase.FindAssets("Stars t:Texture2D");
    var guid = guids[Random.Range(0, guids.Length)];
    var path = AssetDatabase.GUIDToAssetPath(guid);
    var texture = AssetDatabase.LoadAssetAtPath<Texture2D>(path);
    var material = new Material(Shader.Find("Skybox/6 Sided"));
    material.SetTexture("_FrontTex", texture);
    material.SetTexture("_BackTex", texture);
    material.SetTexture("_LeftTex", texture);
    material.SetTexture("_RightTex", texture);
    material.SetTexture("_UpTex", texture);
    material.SetTexture("_DownTex", texture);
    GetComponent<Skybox>().material = material;
  }

  private void Update()
  {
    transform.rotation = Camera.main.transform.rotation;
  }
}
