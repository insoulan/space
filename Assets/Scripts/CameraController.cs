﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
  public float RotationSensitivity = 2f;
  public float MovementSpeed = 1f;
  private Quaternion targetRotation;
  private Quaternion currentRotation;
  private Vector3 targetPosition;
  private Vector3 currentPosition;

  private static float EaseOutExpo(float t)
  {
    return -Mathf.Exp(-10f * t) + 1f;
  }

  private void Start()
  {
    targetRotation = currentRotation = transform.rotation;
    targetPosition = currentPosition = transform.position;
  }

  private void Update()
  {
    var euler = new Vector3(-Input.GetAxisRaw("Mouse Y"),
                             Input.GetAxisRaw("Mouse X"), 0f);
    targetRotation *= Quaternion.Euler(euler * RotationSensitivity);
    if (currentRotation != targetRotation)
    {
      currentRotation = Quaternion.Lerp(currentRotation, targetRotation,
        EaseOutExpo(Time.deltaTime));
      transform.rotation = currentRotation;
    }

    var velocity = new Vector3(Input.GetAxisRaw("Horizontal"), 0f,
                               Input.GetAxisRaw("Vertical"));
    targetPosition += currentRotation * velocity * MovementSpeed;
    if (currentPosition != targetPosition)
    {
      currentPosition = Vector3.Lerp(currentPosition, targetPosition,
        EaseOutExpo(Time.deltaTime));
      transform.position = currentPosition;
    }
  }
}
