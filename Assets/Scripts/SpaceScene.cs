﻿using UnityEditor;
using UnityEngine;

public class SpaceScene : MonoBehaviour
{
  public int NebulaCount = 16;

  private void Start()
  {
    var guids = AssetDatabase.FindAssets("Nebula t:Texture2D");
    var prefab = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/Nebula.prefab");

    for (int i = 0; i < NebulaCount; ++i)
    {
      var guid = guids[Random.Range(0, guids.Length)];
      var path = AssetDatabase.GUIDToAssetPath(guid);
      var texture = AssetDatabase.LoadAssetAtPath<Texture2D>(path);
      var material = new Material(Shader.Find("Particles/Additive"));
      material.SetTexture("_MainTex", texture);

      var nebula = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
      nebula.GetComponent<Renderer>().material = material;
      nebula.layer = gameObject.layer;
      nebula.transform.parent = transform;
      nebula.transform.rotation = Random.rotationUniform;
    }
  }
}
