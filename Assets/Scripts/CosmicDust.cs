﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class CosmicDust : MonoBehaviour
{
  private ParticleSystem particleSystem;
  private ParticleSystem.Particle[] particles;

  void Start()
  {
    particleSystem = GetComponent<ParticleSystem>();
    var main = particleSystem.main;
    var emitParams = new ParticleSystem.EmitParams();
    emitParams.startLifetime = Mathf.Infinity;
    particleSystem.Emit(emitParams, main.maxParticles);
    particles = new ParticleSystem.Particle[main.maxParticles];
  }

  void Update()
  {
    particleSystem.GetParticles(particles);
    var shape = particleSystem.shape;
    for (int i = 0; i < particles.Length; ++i)
    {
      var distance = Vector3.Distance(particles[i].position, transform.position);
      if (distance > shape.radius)
      {
        particles[i].position = transform.position + Random.insideUnitSphere * shape.radius;
        distance = Vector3.Distance(particles[i].position, transform.position);
      }
      float fadeDistance = shape.radius * 0.5f;
      Color color = particles[i].startColor;
      if (distance > fadeDistance)
      {
        particles[i].startColor = new Color(color.r, color.g, color.b,
          1f - Mathf.Clamp01((distance - fadeDistance) / fadeDistance));
      }
      else
      {
        particles[i].startColor = new Color(color.r, color.g, color.b, 1f);
      }
    }
    particleSystem.SetParticles(particles, particles.Length);
  }
}
